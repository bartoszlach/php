<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


use Carbon\Carbon;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface
{

    //Write your methods here

    public function calculate($bookingDateTime, $responseDateTime, $officeHours)
    {
        dd($officeHours);
        $bookingDateTimeParsed = Carbon::parse($bookingDateTime);
        $responseDateTimeParsed = Carbon::parse($responseDateTime);

        $diff = $responseDateTimeParsed->diffForHumans($bookingDateTimeParsed);
        return $diff;

        // TODO: Implement calculate() method.
    }
}
