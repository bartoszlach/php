<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use phpDocumentor\Reflection\Types\Array_;

class DistanceCalculator
{

    /**
     * @param array $from
     * @param array $to
     * @param string $unit - m, km
     *
     * @return mixed
     */
    public function calculate($from, $to, $unit = 'm')
    {
//        var_dump($from, $to, $unit);
        $lat1 = $from[0];
        $lon1 = $from[1];
        $lat2 = $to[0];
        $lon2 = $to[1];
        $rad = M_PI / 180;
        $distance = acos(sin($lat2 * $rad) * sin($lat1 * $rad) + cos($lat2 * $rad) * cos($lat1 * $rad) * cos($lon2 * $rad - $lon1 * $rad)) * 6371;// Kilometers

        if ($unit == 'km')
            return $distance;
        return $distance * 1000;

    }

    /**
     * @param array $from
     * @param array $offices
     *
     * @return array
     */
    public function findClosestOffice($from, $offices)
    {
        $officesWithDistance = [];
        foreach ($offices as $office) {
            $to = [$office['lat'], $office['lng']];
            $office['distance'] = $this->calculate($from, $to);
            array_push($officesWithDistance, $office);

        }
        usort($officesWithDistance, function($a, $b) {
            return $a['distance'] <=> $b['distance'];
        });

        return $officesWithDistance[0];

    }

}
